/*@ predicate QueueInv(Queue q; int n, int m) = 
        q.front |-> ?f
      &*& q.rear |-> ?r
      &*& q.numberOfElements |-> n
      &*& q.maxElements |-> m
      &*& q.elements |-> ?a
      &*& a.length == m
      &*& 0 <= n &*& n <= m
      &*& 0 <= f &*& f < m
      &*& 0 <= r &*& r < m
      &*& array_slice(a,0,m,?items);
@*/

class Queue {
	
  int front;
  int rear;
  int numberOfElements;
  int maxElements;
  int[] elements;

  Queue(int max)
  //@ requires 0 < max;
  //@ ensures QueueInv(this, 0, max);
  {
    this.maxElements = max;
    this.numberOfElements = 0;
    front = 0;
    rear = 0;
    elements = new int[maxElements];
  }


  void enqueue(int v)
  //@ requires QueueInv(this, ?n ,?m) &*& n != m &*& n < m;
  //@ ensures QueueInv(this, n+1, m) &*& n+1 >= 1;
  {
      elements[front] = v;
    	numberOfElements++;
    
      if(front != maxElements -1){
      front++;      
      } else{
        front = 0;
      }  
  }

  int dequeue()
  //@ requires QueueInv(this, ?n ,?m) &*& n == 0 &*& n > 0;
  //@ ensures QueueInv(this, n - 1, m) &*& n-1 <= 0;
  {
     int n = elements[rear];
     elements[rear] = null;
     numberOfElements--;
  
     if(rear != maxElements - 1){
     rear++;
     } else{
       rear = 0;
     }
		 
    return n;
  }
  
  boolean isFull()
  //@ requires QueueInv(this, ?n,?m);
  //@ ensures QueueInv(this, n, m);
  {
  return numberOfElements == maxElements;
  }
  
  boolean isEmpty()
  //@ requires QueueInv(this, ?n, ?m);
  //@ ensures QueueInv(this, n, m);
  {
  return numberOfElements == 0;
  }

}