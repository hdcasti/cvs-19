public class Account {

  int balance;

<<<<<<< HEAD
  /*@ 
  predicate AccountInv(int b) = 
  	this.balance |-> b 
  	&*&
  	b >= 0;
  @*/

  public Account()
  //@ requires true;
  //@ ensures AccountInv(0);
  {
    balance = 0;
  }

  void deposit(int v)
  //@ requires AccountInv(?b) &*& v >= 0;
  //@ ensures AccountInv(b+v);
  {
    balance += v;
  }

  void withdraw(int v)
  //@ requires AccountInv(?b) &*& b >= v;
  //@ ensures AccountInv(b-v);
  {
    balance -= v;
  }

  int getBalance() 
  //@ requires AccountInv(?b);
  //@ ensures AccountInv(b) &*& result==b ; 
  {
    return balance;
  }

  static void transfer(Account a1, Account a2, int v) 
  //@ requires a1.AccountInv(?b1) &*& a2.AccountInv(?b2) &*& v >= 0;
  //@ ensures a1.AccountInv(_) &*& a2.AccountInv(_);
  /* @ ensures v <= b1 ? 
              a1.AccountInv(b1-v) &*& a2.AccountInv(b2+v) 
              : 
              a1.AccountInv(b1) &*& a2.AccountInv(b2);
  @*/
  {
    
    if( v <= a1.getBalance() ) {
      a1.withdraw(v);
      a2.deposit(v);
    }
  }

  public static void main(String[] args) 
  //@ requires true;
  //@ ensures true;
  {
    Account a1 = new Account();
    Account a2 = new Account();
    
    a1.deposit(20);
    transfer(a1,a2,10);
    
    //transfer(a1,a1,20);
  }
=======
  public Account()

  void deposit(int v)

  void withdraw(int v)

  int getBalance()

  static void transfer(Account a1, Account a2, int v)

  public static void main(String[] args)
>>>>>>> upstream/master
}
