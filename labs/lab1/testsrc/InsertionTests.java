import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.jupiter.api.Test;

public class InsertionTests {

    @Test
    public void testPush() {
        final MyIntegerList mil = new MyIntegerList();

        assertEquals(0, mil.size());
        assertEquals("[]", mil.toString());

        mil.push(1);
        assertEquals(1, mil.size());
        assertNotEquals("[]", mil.toString());
        assertEquals("[1]", mil.toString());

        mil.push(2);
        assertEquals(2, mil.size());
        assertEquals("[1,2]", mil.toString());

        mil.push(1);
        assertEquals(3, mil.size());
        assertEquals("[1,2,1]", mil.toString());
    }
    
    @Test
    public void testSortedInsertition() {
    	MyIntegerList mil = generatedPopulatedList();
    	
    	mil.sortedInsertion(0);
    	assertEquals("[0,1,2,3]", mil.toString());
    	
    	mil.sortedInsertion(4);
    	assertEquals("[0,1,2,3,4]", mil.toString());
    }

	
	private MyIntegerList generatedPopulatedList(){
        final MyIntegerList mil = new MyIntegerList();

        mil.push(1);
        mil.push(2);
        mil.push(3);
      
        return mil;
    }

}