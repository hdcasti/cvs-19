import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class OperationsTests {

    private MyIntegerList generatedPopulatedList(){
        final MyIntegerList mil = new MyIntegerList();

        mil.push(1);
        mil.push(2);
        mil.push(1);
        mil.push(6);
        mil.push(6);
        mil.push(7);
        mil.push(2);
        mil.push(2);
        mil.push(0);
        mil.push(5);

        return mil;
    }
    
    private MyIntegerList generatedList(){
        final MyIntegerList mil = new MyIntegerList();

        mil.push(4);
        mil.push(5);
        mil.push(3);


        return mil;
    }

    @Test
    void testSort() {
        final MyIntegerList mil = generatedPopulatedList();
        assertEquals(10, mil.size());
        assertEquals("[1,2,1,6,6,7,2,2,0,5]", mil.toString());


        mil.bubbleSort();
        assertEquals(10, mil.size());
        assertEquals("[0,1,1,2,2,2,5,6,6,7]", mil.toString());
    }
    
    @Test
    public void testSum() {
    	MyIntegerList mil = generatedList(); 
    	
    	int sum = mil.elementsSum();
    	assertEquals(12, sum);
    	
    }
    
    @Test
    public void testBubble() {
    	MyIntegerList mil = generatedList();
    	
    	mil.bubbleSort();
    	assertEquals("[3,4,5]", mil.toString());
    }
    
    @Test
    public void testDifferent() {
    	final MyIntegerList mil = generatedPopulatedList();
    	
    	int d = mil.countDifferent();
    	assertEquals(3, d);
    }

}