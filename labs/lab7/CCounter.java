package lab7;
import java.util.concurrent.*;
import java.util.concurrent.locks.*;


/*@

predicate_ctor CCounter(CCounter c, int v, int m)() = 
    c.counter |-> ?s
    &*& s != null
    &*& BCounter(s,?v,?m);

predicate_ctor CCounterSharedState(CCounter c)() = 
    c.counter |-> ?s
    &*& s != null
    &*& BCounter(s,_,_);

predicate_ctor CCounterSharedStateNotZero(CCounter c)() = 
    c.counter |-> ?s
    &*& s != null
    &*& BCounter(s,?v,_)
    &*& v > 0;

predicate_ctor CCounterSharedStateNotMax(CCounter c)() = 
    c.counter |-> ?s
    &*& s != null
    &*& BCounter(s,?v,?m)
    &*& v < m;

predicate CCounterInv(CCounter c) = 
    c.mon |-> ?l
    &*& c.monZero |-> ?cz
    &*& c.monMax |-> ?cm
    &*& l != null
    &*& cz != null
    &*& cm != null
    &*& lck(l, l, CCounterSharedState(c))
    cond(cz, CCounterSharedState(c),CCountSharedStateNotZero(c))
    cond(cm, CCounterSharedState(c),CCountSharedStateNotZero(c));
@*/

class CCounter {
    BCounter counter;
    ReentrantLock mon;
    Condition monZero;
    Condition monMax;

    CCounter(int max)
        //@ requires 0 <= max;
        //@ ensures CCounterInv(this);
    {
        this.counter = BCounter(max);
        //@ close CCounterSharedState(this)();
        //@ close enter_lck(l,CCounterSharedState(this));
        //@ close set_cond(CCounterSharedState(this), CCounterSharedNotZero(this))
        mon = new ReentrantLock();
        monZero = mon.newCondition();
        monMax = mon.newCondition();
        //@ close CCounterInv(this);
    }

    void inc()
        //@ requires CCounterInv(this)
        //@ ensures true;
        {
        //@ open CCounter(this);
        mon.lock(); 

        //@ open CCounterSharedState(this)();
        if(counter.get() == counter.getMax()){ 
            //@ close CCounterSharedState(this)();
            nonMax.await(); 
            // @ open CCounterSharedState(this)();
        }
        this.counter.inc(); 
          //@ close CCounterSharedState(this)();
        mon.unlock(); 
        //@ close CCounterInv(this);
    }

     void dec(){
        mon.lock(); 
        this.counter.dec(); 
        mon.unlock(); 
    }
}