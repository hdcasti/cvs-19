import java.util.concurrent.locks.*;

/*@

predicate CounterP(CCounter c, int v, int m) = 
	        c.counter |-> ?s 
	    &*& s != null
	    &*& BCounterInv(s, v, m);


predicate_ctor CCounterSharedState(CCounter c)() =
	        c.counter |-> ?s 
	    &*& s != null
	    &*& BCounterInv(s, _, _);

predicate_ctor CCounterSharedStateNotZero(CCounter c)() =
	        c.counter |-> ?s 
	    &*& s != null
	    &*& BCounterInv(s, ?v, _)
	    &*& v > 0;

predicate_ctor CCounterSharedStateNotMax(CCounter c)() =
	        c.counter |-> ?s 
	    &*& s != null
	    &*& BCounterInv(s, ?v, ?m)
	    &*& v < m;

predicate CCounterInv(CCounter c) =
	    c.mon |-> ?l
	&*& c.nonZero |-> ?cz
	&*& c.nonMax  |-> ?cm
	&*& l != null
	&*& cz != null
	&*& cm != null
	&*& lck(l, 1, CCounterSharedState(c))
	&*& cond(cz,CCounterSharedState(c),CCounterSharedStateNotZero(c))
	&*& cond(cm,CCounterSharedState(c),CCounterSharedStateNotMax(c));
	
@*/

class CCounter {

	BCounter counter;
	ReentrantLock mon;
	Condition nonZero;
	Condition nonMax;
	
	CCounter(int max) 
	//@ requires 0 <= max;
	//@ ensures CCounterInv(this);
	{
		counter = new BCounter(max);
		//@ close CCounterSharedState(this)();
		//@ close enter_lck(1,CCounterSharedState(this));
		mon = new ReentrantLock();

		//@ close set_cond(CCounterSharedState(this), CCounterSharedStateNotZero(this));
		nonZero = mon.newCondition();
		//@ close set_cond(CCounterSharedState(this), CCounterSharedStateNotMax(this));
		nonMax = mon.newCondition();
		//@ close CCounterInv(this);
	}
	
	void inc() 
	//@ requires CCounterInv(this);
	//@ ensures CCounterInv(this);
	{
		//@ open CCounterInv(this);
		mon.lock();
		//@ open CCounterSharedState(this)();
		if( counter.get() == counter.getMax()) {
		  //@ close CCounterSharedState(this)();
		  nonMax.await();
		  //@ open CCounterSharedStateNotMax(this)();
		}
		//@ open BCounterInv(counter,_,_);
		this.counter.inc();
		//@ close CCounterSharedStateNotZero(this)();		
		nonZero.signal();
		mon.unlock();
		//@ close CCounterInv(this);
	}

	void dec() 
	//@ requires CCounterInv(this);
	//@ ensures CCounterInv(this);	
	{
		//@ open CCounterInv(this);
		mon.lock();
		//@ open CCounterSharedState(this)();
		if( counter.get() == 0) {
		  //@ close CCounterSharedState(this)();
		  nonZero.await();
		  //@ open CCounterSharedStateNotZero(this)();
		}
		//@ open BCounterInv(counter,_,_);
		this.counter.dec();
		//@ close CCounterSharedStateNotMax(this)();
		nonMax.signal();
		mon.unlock();
		//@ close CCounterInv(this);
	}
	
  	int get()
  	//@ requires CCounterInv(this);
  	//@ ensures CCounterInv(this);
  	{
  		int n;
		//@ open CCounterInv(this);
		mon.lock();
		//@ open CCounterSharedState(this)();
  		n = counter.get();
  		//@ close CCounterSharedState(this)();
  		mon.unlock();
  		return n; 
		//@ close CCounterInv(this);  		
  	} 

  	int getMax()
  	//@ requires CCounterInv(this);
  	//@ ensures CCounterInv(this);
  	{
  		int m;
		//@ open CCounterInv(this);
		mon.lock();
		//@ open CCounterSharedState(this)();
  		m = counter.get();
  		//@ close CCounterSharedState(this)();
  		mon.unlock();
  		return m; 
		//@ close CCounterInv(this);  		
  		
  	}
}